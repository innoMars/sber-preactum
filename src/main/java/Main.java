import service.CityService;
import service.CityServiceImpl;

public class Main {
    public static void main(String[] args) {
        CityService cityService = new CityServiceImpl();

        cityService.printCountCitiesInRegions();
    }
}
