package util;

import model.City;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public final class CityManager {
   private final static Path FILE_PATH = Paths.get("src", "main", "resources", "Задача ВС Java Сбер.csv");

    private CityManager() {
    }

    public static List<City> getCitiesFromFile() {
        List<City> cities = new ArrayList<>();

        try (Scanner scanner = new Scanner(getFile(), StandardCharsets.UTF_8)) {

            while (scanner.hasNextLine()) {
                cities.add(parseCity(scanner.nextLine()));
            }
        } catch (Exception e) {
            throw new RuntimeException("Ошибка чтения файла: " + e);
        }

        return cities;
    }

    private static City parseCity(String line) {
        try (Scanner scanner = new Scanner(line)) {
            scanner.useDelimiter(";");
            scanner.skip("\\d*");
            String name = scanner.next();
            String region = scanner.next();
            String district = scanner.next();
            Integer population = scanner.nextInt();
            String foundation = null;

            if (scanner.hasNext()) {
                foundation = scanner.next();
            }

            return new City(name, region, district, population, foundation);
        }
    }

    private static File getFile() {
        File file = FILE_PATH.toFile();
        return file;
    }
}
