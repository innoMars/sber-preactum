package service;

import model.City;

import java.util.List;
import java.util.Map;

public interface CityService {
    List<City> getAllCity();

    List<City> sortedByNameWithoutRegisterDesc();

    List<City> sortedByDistinctAndNameWithRegisterDesc();

    String getIndexCityAndPopulationMax();

    Map<String, Long> getCountCitiesInRegionMap();

    void printCountCitiesInRegions();
}
