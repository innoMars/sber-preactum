package service;

import model.City;
import util.CityManager;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CityServiceImpl implements CityService {

    private final List<City> cities = CityManager.getCitiesFromFile();

    @Override
    public List<City> getAllCity() {
        return cities;
    }

    @Override
    public List<City> sortedByNameWithoutRegisterDesc() {

        return cities.stream()
                .sorted((c1, c2) -> c2.getName().compareToIgnoreCase(c1.getName()))
                .collect(Collectors.toList());
    }

    @Override
    public List<City> sortedByDistinctAndNameWithRegisterDesc() {
        return cities.stream()
                .sorted(Comparator.comparing(City::getDistrict)
                        .thenComparing(Comparator.comparing(City::getName).reversed()))
                .collect(Collectors.toList());
    }

    @Override
    public String getIndexCityAndPopulationMax() {
        City[] citiesArray = cities.toArray(new City[0]);

        int maxPopulationIndex = IntStream.range(0, citiesArray.length)
                .boxed()
                .max(Comparator.comparingInt(i -> citiesArray[i].getPopulation()))
                .orElse(-1);

        int maxPopulation = citiesArray[maxPopulationIndex].getPopulation();

        return String.format("[%d] = %d", maxPopulationIndex, maxPopulation);
    }

    @Override
    public Map<String, Long> getCountCitiesInRegionMap() {
        return cities.stream()
                .collect(Collectors.groupingBy(City::getRegion, Collectors.counting()));
    }

    @Override
    public void printCountCitiesInRegions() {
        getCountCitiesInRegionMap().forEach((region, count) -> System.out.println(region + " - " + count));
    }
}
